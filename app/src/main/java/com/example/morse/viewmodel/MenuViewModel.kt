package com.example.morse.viewmodel

import androidx.lifecycle.ViewModel
import com.example.morse.navigation.ScreenPool
import ru.terrakok.cicerone.Router

class MenuViewModel(private val router: Router) : ViewModel() {

    fun goToWritingFragment() {
        router.navigateTo(ScreenPool.WritingScreen())
    }

    fun goToStaminaFragment() {
        router.navigateTo(ScreenPool.StaminaScreen())
    }

    fun goToABCFragment() {
        router.navigateTo(ScreenPool.ABCScreen())
    }

    fun goToTest() {
        router.navigateTo(ScreenPool.TestScreen())
    }
}