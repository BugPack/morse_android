package com.example.morse.viewmodel

import android.text.Editable
import android.text.TextWatcher
import android.widget.SeekBar
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.example.morse.utils.MorseHelper
import com.example.morse.common.App
import com.example.morse.utils.SoundGenerator

class WritingViewModel(application: App) : AndroidViewModel(application) {

    private val soundGenerator = SoundGenerator()

    val inputText = ObservableField<String>()
    val morse = ObservableField<String>()
    val volumeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            soundGenerator.setVolume(progress)
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {}

        override fun onStopTrackingTouch(seekBar: SeekBar?) {}
    }

    val textListener = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            morse.set(MorseHelper.textToMorse(s.toString()))
        }
    }

    fun playText() {
        inputText.get()?.let {
            soundGenerator.playSoundSet(getApplication(), MorseHelper.textToSoundList(it))
        }
    }
}