package com.example.morse.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.morse.entity.LanguageEnum
import com.example.morse.entity.LetterEntity
import com.example.morse.ui.adapter.ABCAdapter
import com.example.morse.utils.MorseHelper
import com.example.morse.utils.sourceEng
import com.example.morse.utils.sourceRu

class ABCViewModel : ViewModel() {
    var language = LanguageEnum.ENG
    val abcAdapter = ABCAdapter()
    val spanCount = ObservableField(4)

    fun changeLanguage(language: LanguageEnum) {
        val result = arrayListOf<LetterEntity>()
        when (language) {
            LanguageEnum.ENG -> {
                this.language = LanguageEnum.ENG
                sourceEng.forEach {
                    result.add(
                        LetterEntity(
                            it.toString(), MorseHelper.letterToMorse(it.toString())
                        )
                    )
                }
            }
            LanguageEnum.RU -> {
                this.language = LanguageEnum.RU
                sourceRu.forEach {
                    result.add(
                        LetterEntity(
                            it.toString(), MorseHelper.letterToMorse(it.toString())
                        )
                    )
                }
            }
        }
        abcAdapter.addItems(result)
    }

    fun checkLanguage() {
        if (language == LanguageEnum.ENG) {
            changeLanguage(LanguageEnum.RU)
        } else {
            changeLanguage(LanguageEnum.ENG)
        }
    }
}