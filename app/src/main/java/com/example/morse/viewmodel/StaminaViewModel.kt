package com.example.morse.viewmodel

import android.content.Context
import android.widget.SeekBar
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.example.morse.entity.ButtonEntity
import com.example.morse.ui.adapter.ButtonsAdapter
import com.example.morse.utils.MorseHelper
import com.example.morse.utils.SoundGenerator
import com.example.morse.utils.StaminaGame
import com.example.morse.utils.source

class StaminaViewModel(private val context: Context) : ViewModel() {
    val staminaGame = StaminaGame()
    private val soundGenerator = SoundGenerator()

    val gameInfo = ObservableField<String>("win: 0  loses: 0")
    val progress = ObservableField<Int>()
    val letterMorse = ObservableField<String>()
    val letter = ObservableField<String>()
    val progressText = ObservableField<String>()
    val adapter = ButtonsAdapter()
    val isLetterVisible = ObservableBoolean(false)
    val spanCount = ObservableInt(4)

    val volumeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            soundGenerator.setVolume(progress)
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {}

        override fun onStopTrackingTouch(seekBar: SeekBar?) {}
    }

    fun checkLetter(letter: String) {
        staminaGame.checkLetter(letter)
    }

    fun repeatMorse() {
        letter.get()?.let {
            soundGenerator.playSoundSet(context, MorseHelper.textToSoundList(it))
        }
    }

    fun triggerLanguage() {
        staminaGame.triggerLanguage()
        generateButtons()
    }

    fun generateButtons() {
        val list = arrayListOf<ButtonEntity>()
        for (i in source.indices) {
            val button = ButtonEntity(source[i].toString()) {
                checkLetter(source[i].toString())
            }
            list.add(button)
        }
        adapter.addAll(list)
    }
}