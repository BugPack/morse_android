package com.example.morse.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.morse.entity.ButtonEntity
import com.example.morse.entity.LetterEntity
import com.example.morse.ui.adapter.ButtonsAdapter
import com.example.morse.utils.TestGame

class TestViewModel : ViewModel() {
    val adapter = ButtonsAdapter()
    val testGame = TestGame()
    val letter = ObservableField<String>()
    val spanCount = ObservableField(2)

    fun generateButtons(list: List<LetterEntity>) {
        val buttons = arrayListOf<ButtonEntity>()
        list.forEach {
            buttons.add(ButtonEntity(it.letterMorse) {
                testGame.checkAnswer(it)
            })
        }
        adapter.addAll(buttons)
    }
}