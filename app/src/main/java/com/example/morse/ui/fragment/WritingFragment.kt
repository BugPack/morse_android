package com.example.morse.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.morse.R
import com.example.morse.databinding.FragmentWritingBinding
import com.example.morse.viewmodel.WritingViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class WritingFragment : Fragment() {
    companion object {
        fun getInstance() = WritingFragment()
    }

    private val viewModel: WritingViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentWritingBinding>(
            inflater,
            R.layout.fragment_writing,
            container,
            false
        ).apply { viewModel = this@WritingFragment.viewModel }.root
    }
}