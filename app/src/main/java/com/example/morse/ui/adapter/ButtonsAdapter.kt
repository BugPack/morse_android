package com.example.morse.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.morse.R
import com.example.morse.entity.ButtonEntity
import kotlinx.android.synthetic.main.list_item_button.view.*

class ButtonsAdapter : RecyclerView.Adapter<ButtonsAdapter.ButtonsViewHolder>() {
    private val dataset = arrayListOf<ButtonEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ButtonsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_button, parent, false)
        )

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: ButtonsViewHolder, position: Int) {
        val item = dataset[position]
        item.apply {
            holder.button.text = item.text
            holder.button.setOnClickListener {
                item.onClick()
            }
        }
    }

    fun addAll(items: List<ButtonEntity>) {
        dataset.clear()
        dataset.addAll(items)
        notifyDataSetChanged()
    }

    class ButtonsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val button = view.btn
    }
}