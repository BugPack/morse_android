package com.example.morse.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.morse.R
import com.example.morse.entity.LetterEntity
import kotlinx.android.synthetic.main.list_item_letter.view.*

class ABCAdapter : RecyclerView.Adapter<ABCAdapter.ABCViewHolder>() {
    private val dataset = arrayListOf<LetterEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ABCViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.list_item_letter, parent, false)
    )

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: ABCViewHolder, position: Int) {
        val item = dataset[position]
        holder.apply {
            letter.text = item.letter
            letterMorse.text = item.letterMorse
        }
    }

    fun addItems(list: List<LetterEntity>) {
        dataset.clear()
        dataset.addAll(list)
        notifyDataSetChanged()
    }

    class ABCViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val letter = view.tvLetter
        val letterMorse = view.tvLetterMorse
    }

}