package com.example.morse.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.morse.R
import com.example.morse.databinding.FragmentTestBinding
import com.example.morse.viewmodel.TestViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TestFragment : Fragment() {

    companion object {
        fun getInstance() = TestFragment()
    }

    private val viewModel: TestViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentTestBinding>(
            inflater,
            R.layout.fragment_test,
            container,
            false
        ).apply { viewModel = this@TestFragment.viewModel }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startGame()
    }

    private fun startGame() {
        viewModel.testGame.setLetterCallback {
            viewModel.letter.set(it)
        }
        viewModel.testGame.setAnswersCallback { list ->
            viewModel.generateButtons(list)
        }
        viewModel.testGame.setResultCallback {
            if (it) {
                viewModel.testGame.startGame()
            } else {
                Toast.makeText(context, "Попробуй снова", Toast.LENGTH_SHORT).show()
            }
        }
        viewModel.testGame.startGame()
    }
}