package com.example.morse.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.morse.R
import com.example.morse.databinding.FragmentAbcBinding
import com.example.morse.entity.LanguageEnum
import com.example.morse.viewmodel.ABCViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ABCFragment : Fragment() {

    companion object {
        fun getInstance() = ABCFragment()
    }

    private val viewModel: ABCViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentAbcBinding>(
            inflater,
            R.layout.fragment_abc,
            container,
            false
        ).apply { viewModel = this@ABCFragment.viewModel }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.changeLanguage(LanguageEnum.ENG)
    }
}