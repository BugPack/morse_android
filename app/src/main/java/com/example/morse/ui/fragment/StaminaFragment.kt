package com.example.morse.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.morse.R
import com.example.morse.databinding.FragmentStaminaBinding
import com.example.morse.viewmodel.StaminaViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class StaminaFragment : Fragment() {

    companion object {
        fun getInstance() = StaminaFragment()
    }

    private val viewModel: StaminaViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return DataBindingUtil.inflate<FragmentStaminaBinding>(
            inflater,
            R.layout.fragment_stamina,
            container,
            false
        ).apply {
            viewModel = this@StaminaFragment.viewModel
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.generateButtons()
    }

    override fun onResume() {
        super.onResume()
        viewModel.staminaGame.apply {
            setLetterCallback { letter, morse ->
                viewModel.letterMorse.set(morse)
                viewModel.letter.set(letter)
                context?.let {
                    viewModel.repeatMorse()
                }
            }
            setProgressCallback {
                viewModel.progress.set(it)
                viewModel.progressText.set((it / 10).toString())
            }
            setResultCallback {
                if (it) {
                    Toast.makeText(context, "Верно", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(context, "Не угадал", Toast.LENGTH_SHORT).show()
                    viewModel.isLetterVisible.set(true)
                }
            }
            setGameInfoCallback {
                viewModel.gameInfo.set(it)
            }
            setNewGameCallback {
                viewModel.isLetterVisible.set(false)
            }
            startGame()
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel.staminaGame.stopGame()
    }
}