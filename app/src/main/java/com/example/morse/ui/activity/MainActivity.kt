package com.example.morse.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.morse.R
import com.example.morse.navigation.ScreenPool
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val navigationHolder: NavigatorHolder by inject()
    private val router: Router by inject()

    private val navigator = object : SupportAppNavigator(this, R.id.container) {
        override fun applyCommand(command: Command?) {
            super.applyCommand(command)
            supportFragmentManager.executePendingTransactions()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        router.newRootScreen(ScreenPool.MenuScreen())
    }

    override fun onResume() {
        super.onResume()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigationHolder.removeNavigator()
    }
}