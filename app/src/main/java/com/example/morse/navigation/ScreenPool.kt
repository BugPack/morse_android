package com.example.morse.navigation

import com.example.morse.ui.fragment.*
import ru.terrakok.cicerone.android.support.SupportAppScreen

object ScreenPool {

    class MenuScreen : SupportAppScreen() {
        override fun getFragment() = MenuFragment.getInstance()
    }

    class ABCScreen : SupportAppScreen() {
        override fun getFragment() = ABCFragment.getInstance()
    }

    class WritingScreen : SupportAppScreen() {
        override fun getFragment() = WritingFragment.getInstance()
    }

    class StaminaScreen : SupportAppScreen() {
        override fun getFragment() = StaminaFragment.getInstance()
    }

    class TestScreen : SupportAppScreen() {
        override fun getFragment() = TestFragment.getInstance()
    }
}