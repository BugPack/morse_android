package com.example.morse.common

import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.SeekBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.morse.ui.adapter.ABCAdapter
import com.example.morse.ui.adapter.ButtonsAdapter

@BindingAdapter("textChangedListener")
fun EditText.textChangedListener(watcher: TextWatcher) {
    this.addTextChangedListener(watcher)
}

@BindingAdapter("onProgressChanged")
fun SeekBar.onProgressChanged(listener: SeekBar.OnSeekBarChangeListener) {
    this.setOnSeekBarChangeListener(listener)
}

@BindingAdapter("setButtonsAdapter")
fun RecyclerView.setButtonsAdapter(adapter: ButtonsAdapter) {
    this.adapter = adapter
}

@BindingAdapter("setABCAdapter")
fun RecyclerView.setABCAdapter(adapter: ABCAdapter) {
    this.adapter = adapter
}

@BindingAdapter("gridManager")
fun RecyclerView.gridManager(spanCount: Int) {
    this.layoutManager = GridLayoutManager(this.context, spanCount)
}

@BindingAdapter("isLetterVisible")
fun View.isLetterVisible(isVisible: Boolean) {
    if (isVisible) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}