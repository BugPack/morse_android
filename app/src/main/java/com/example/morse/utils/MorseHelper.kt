package com.example.morse.utils

import com.example.morse.R
import com.example.morse.entity.MorseCodeEnum

object MorseHelper {

    fun textToSoundList(text: String): ArrayList<SoundGenerator.Sound> {
        val result = arrayListOf<SoundGenerator.Sound>()
        if (text.isNotEmpty()) {
            text.toLowerCase().forEach {
                var charStr = it.toString()
                when (charStr) {
                    "ё" -> charStr = "е"
                    "ъ" -> charStr = "ь"
                }

                when (charStr) {
                    MorseCodeEnum.A.namEng,
                    MorseCodeEnum.A.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.a_morse_code, MorseCodeEnum.A.duration
                    )) }
                    MorseCodeEnum.Ä.namEng,
                    MorseCodeEnum.Ä.namRu -> {  }
                    MorseCodeEnum.B.namEng,
                    MorseCodeEnum.B.namRu-> { result.add(SoundGenerator.Sound(
                        R.raw.b_morse_code, MorseCodeEnum.B.duration
                    )) }
                    MorseCodeEnum.C.namEng,
                    MorseCodeEnum.C.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.c_morse_code, MorseCodeEnum.C.duration
                    )) }
                    MorseCodeEnum.CH.namEng,
                    MorseCodeEnum.CH.namRu -> {  }
                    MorseCodeEnum.D.namEng,
                    MorseCodeEnum.D.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.d_morse_code, MorseCodeEnum.D.duration
                    )) }
                    MorseCodeEnum.E.namEng,
                    MorseCodeEnum.E.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.e_morse_code, MorseCodeEnum.E.duration
                    )) }
                    MorseCodeEnum.É.namEng,
                    MorseCodeEnum.É.namRu -> {  }
                    MorseCodeEnum.F.namEng,
                    MorseCodeEnum.F.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.f_morse_code, MorseCodeEnum.F.duration
                    )) }
                    MorseCodeEnum.G.namEng,
                    MorseCodeEnum.G.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.g_morse_code, MorseCodeEnum.G.duration
                    )) }
                    MorseCodeEnum.H.namEng,
                    MorseCodeEnum.H.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.h_morse_code, MorseCodeEnum.H.duration
                    )) }
                    MorseCodeEnum.I.namEng,
                    MorseCodeEnum.I.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.i_morse_code, MorseCodeEnum.I.duration
                    )) }
                    MorseCodeEnum.J.namEng,
                    MorseCodeEnum.J.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.j_morse_code, MorseCodeEnum.J.duration
                    )) }
                    MorseCodeEnum.K.namEng,
                    MorseCodeEnum.K.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.k_morse_code, MorseCodeEnum.K.duration
                    )) }
                    MorseCodeEnum.L.namEng,
                    MorseCodeEnum.L.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.l_morse_code, MorseCodeEnum.L.duration
                    )) }
                    MorseCodeEnum.M.namEng,
                    MorseCodeEnum.M.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.m_morse_code, MorseCodeEnum.M.duration
                    )) }
                    MorseCodeEnum.N.namEng,
                    MorseCodeEnum.N.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.n_morse_code, MorseCodeEnum.N.duration
                    )) }
                    MorseCodeEnum.O.namEng,
                    MorseCodeEnum.O.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.o_morse_code, MorseCodeEnum.O.duration
                    )) }
                    MorseCodeEnum.Ö.namEng,
                    MorseCodeEnum.Ö.namRu -> {  }
                    MorseCodeEnum.P.namEng,
                    MorseCodeEnum.P.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.p_morse_code, MorseCodeEnum.P.duration
                    )) }
                    MorseCodeEnum.Q.namEng,
                    MorseCodeEnum.Q.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.q_morse_code, MorseCodeEnum.Q.duration
                    )) }
                    MorseCodeEnum.R.namEng,
                    MorseCodeEnum.R.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.r_morse_code, MorseCodeEnum.R.duration
                    )) }
                    MorseCodeEnum.S.namEng,
                    MorseCodeEnum.S.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.s_morse_code, MorseCodeEnum.S.duration
                    )) }
                    MorseCodeEnum.T.namEng,
                    MorseCodeEnum.T.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.t_morse_code, MorseCodeEnum.T.duration
                    )) }
                    MorseCodeEnum.U.namEng,
                    MorseCodeEnum.U.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.u_morse_code, MorseCodeEnum.U.duration
                    )) }
                    MorseCodeEnum.Ü.namEng,
                    MorseCodeEnum.Ü.namRu -> {  }
                    MorseCodeEnum.V.namEng,
                    MorseCodeEnum.V.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.v_morse_code, MorseCodeEnum.V.duration
                    )) }
                    MorseCodeEnum.W.namEng,
                    MorseCodeEnum.W.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.w_morse_code, MorseCodeEnum.W.duration
                    )) }
                    MorseCodeEnum.X.namEng,
                    MorseCodeEnum.X.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.x_morse_code, MorseCodeEnum.X.duration
                    )) }
                    MorseCodeEnum.Y.namEng,
                    MorseCodeEnum.Y.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.y_morse_code, MorseCodeEnum.Y.duration
                    )) }
                    MorseCodeEnum.Z.namEng,
                    MorseCodeEnum.Z.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw.z_morse_code, MorseCodeEnum.Z.duration
                    )) }
                    MorseCodeEnum._1.namEng,
                    MorseCodeEnum._1.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._1_number_morse_code, MorseCodeEnum._1.duration
                    )) }
                    MorseCodeEnum._2.namEng,
                    MorseCodeEnum._2.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._2_number_morse_code, MorseCodeEnum._2.duration
                    )) }
                    MorseCodeEnum._3.namEng,
                    MorseCodeEnum._3.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._3_number_morse_code, MorseCodeEnum._3.duration
                    )) }
                    MorseCodeEnum._4.namEng,
                    MorseCodeEnum._4.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._4_number_morse_code, MorseCodeEnum._4.duration
                    )) }
                    MorseCodeEnum._5.namEng,
                    MorseCodeEnum._5.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._5_number_morse_code, MorseCodeEnum._5.duration
                    )) }
                    MorseCodeEnum._6.namEng,
                    MorseCodeEnum._6.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._6_number_morse_code, MorseCodeEnum._6.duration
                    )) }
                    MorseCodeEnum._7.namEng,
                    MorseCodeEnum._7.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._7_number_morse_code, MorseCodeEnum._7.duration
                    )) }
                    MorseCodeEnum._8.namEng,
                    MorseCodeEnum._8.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._8_number_morse_code, MorseCodeEnum._8.duration
                    )) }
                    MorseCodeEnum._9.namEng,
                    MorseCodeEnum._9.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._9_number_morse_code, MorseCodeEnum._9.duration
                    )) }
                    MorseCodeEnum._0.namEng,
                    MorseCodeEnum._0.namRu -> { result.add(SoundGenerator.Sound(
                        R.raw._0_number_morse_code, MorseCodeEnum._0.duration
                    )) }
                    MorseCodeEnum.DOT.namEng,
                    MorseCodeEnum.DOT.namRu -> {  }
                    MorseCodeEnum.COMMA.namEng,
                    MorseCodeEnum.COMMA.namRu -> {  }
                    MorseCodeEnum.COLON.namEng,
                    MorseCodeEnum.COLON.namRu -> {  }
                    MorseCodeEnum.SEMICOLON.namEng,
                    MorseCodeEnum.SEMICOLON.namRu -> {  }
                    MorseCodeEnum.BRACKET_OPEN.namEng,
                    MorseCodeEnum.BRACKET_OPEN.namRu -> {  }
                    MorseCodeEnum.BRACKET_CLOSE.namEng,
                    MorseCodeEnum.BRACKET_CLOSE.namRu -> {  }
                    MorseCodeEnum.APOSTROPHE.namEng,
                    MorseCodeEnum.APOSTROPHE.namRu -> {  }
                    MorseCodeEnum.QUOTES.namEng,
                    MorseCodeEnum.QUOTES.namRu -> {  }
                    MorseCodeEnum.QUOTES_BRACKET_OPEN.namEng,
                    MorseCodeEnum.QUOTES_BRACKET_OPEN.namRu -> {  }
                    MorseCodeEnum.QUOTES_BRACKET_CLOSE.namEng,
                    MorseCodeEnum.QUOTES_BRACKET_CLOSE.namRu -> {  }
                    MorseCodeEnum.DASH.namEng,
                    MorseCodeEnum.DASH.namRu -> {  }
                    MorseCodeEnum.SLASH.namEng,
                    MorseCodeEnum.SLASH.namRu -> {  }
                    MorseCodeEnum.UNDERLINE.namEng,
                    MorseCodeEnum.UNDERLINE.namRu -> {  }
                    MorseCodeEnum.QUESTION_MARK.namEng,
                    MorseCodeEnum.QUESTION_MARK.namRu -> {  }
                    MorseCodeEnum.EXCLAMATION_MARK.namEng,
                    MorseCodeEnum.EXCLAMATION_MARK.namRu -> {  }
                    MorseCodeEnum.PLUS.namEng,
                    MorseCodeEnum.PLUS.namRu -> {  }
                    MorseCodeEnum.SECTION_SIGN.namEng,
                    MorseCodeEnum.SECTION_SIGN.namRu -> {  }
                    MorseCodeEnum._DOG.namEng,
                    MorseCodeEnum._DOG.namRu -> {  }
                }
            }
        }
        return result
    }

    fun textToMorse(text: String): String {
        var result = ""
        if (text.isNotEmpty()) {
            text.toLowerCase().forEach {
                result += letterToMorse(it.toString())
                result += " "
            }
        }
        return result
    }

    fun letterToMorse(letter: String): String {
        var result = ""

        var charStr = letter
        when (charStr) {
            "ё" -> charStr = "е"
            "ъ" -> charStr = "ь"
        }

        when (charStr) {
            MorseCodeEnum.A.namEng,
            MorseCodeEnum.A.namRu -> { result += MorseCodeEnum.A.code }
            MorseCodeEnum.Ä.namEng,
            MorseCodeEnum.Ä.namRu -> { result += MorseCodeEnum.Ä.code }
            MorseCodeEnum.B.namEng,
            MorseCodeEnum.B.namRu -> { result += MorseCodeEnum.B.code }
            MorseCodeEnum.C.namEng,
            MorseCodeEnum.C.namRu -> { result += MorseCodeEnum.C.code }
            // todo this bug, forEach take one symbol
            MorseCodeEnum.CH.namEng,
            MorseCodeEnum.CH.namRu -> { result += MorseCodeEnum.CH.code }
            MorseCodeEnum.D.namEng,
            MorseCodeEnum.D.namRu -> { result += MorseCodeEnum.D.code }
            MorseCodeEnum.E.namEng,
            MorseCodeEnum.E.namRu -> { result += MorseCodeEnum.E.code }
            MorseCodeEnum.É.namEng,
            MorseCodeEnum.É.namRu -> { result += MorseCodeEnum.É.code }
            MorseCodeEnum.F.namEng,
            MorseCodeEnum.F.namRu -> { result += MorseCodeEnum.F.code }
            MorseCodeEnum.G.namEng,
            MorseCodeEnum.G.namRu -> { result += MorseCodeEnum.G.code }
            MorseCodeEnum.H.namEng,
            MorseCodeEnum.H.namRu -> { result += MorseCodeEnum.H.code }
            MorseCodeEnum.I.namEng,
            MorseCodeEnum.I.namRu -> { result += MorseCodeEnum.I.code }
            MorseCodeEnum.J.namEng,
            MorseCodeEnum.J.namRu -> { result += MorseCodeEnum.J.code }
            MorseCodeEnum.K.namEng,
            MorseCodeEnum.K.namRu -> { result += MorseCodeEnum.K.code }
            MorseCodeEnum.L.namEng,
            MorseCodeEnum.L.namRu -> { result += MorseCodeEnum.L.code }
            MorseCodeEnum.M.namEng,
            MorseCodeEnum.M.namRu -> { result += MorseCodeEnum.M.code }
            MorseCodeEnum.N.namEng,
            MorseCodeEnum.N.namRu -> { result += MorseCodeEnum.N.code }
            MorseCodeEnum.O.namEng,
            MorseCodeEnum.O.namRu -> { result += MorseCodeEnum.O.code }
            MorseCodeEnum.Ö.namEng,
            MorseCodeEnum.Ö.namRu -> { result += MorseCodeEnum.Ö.code }
            MorseCodeEnum.P.namEng,
            MorseCodeEnum.P.namRu -> { result += MorseCodeEnum.P.code }
            MorseCodeEnum.Q.namEng,
            MorseCodeEnum.Q.namRu -> { result += MorseCodeEnum.Q.code }
            MorseCodeEnum.R.namEng,
            MorseCodeEnum.R.namRu -> { result += MorseCodeEnum.R.code }
            MorseCodeEnum.S.namEng,
            MorseCodeEnum.S.namRu -> { result += MorseCodeEnum.S.code }
            MorseCodeEnum.T.namEng,
            MorseCodeEnum.T.namRu -> { result += MorseCodeEnum.T.code }
            MorseCodeEnum.U.namEng,
            MorseCodeEnum.U.namRu -> { result += MorseCodeEnum.U.code }
            MorseCodeEnum.Ü.namEng,
            MorseCodeEnum.Ü.namRu -> { result += MorseCodeEnum.Ü.code }
            MorseCodeEnum.V.namEng,
            MorseCodeEnum.V.namRu -> { result += MorseCodeEnum.V.code }
            MorseCodeEnum.W.namEng,
            MorseCodeEnum.W.namRu -> { result += MorseCodeEnum.W.code }
            MorseCodeEnum.X.namEng,
            MorseCodeEnum.X.namRu -> { result += MorseCodeEnum.X.code }
            MorseCodeEnum.É.namEng,
            MorseCodeEnum.É.namRu -> { result += MorseCodeEnum.É.code }
            MorseCodeEnum.Ö.namEng,
            MorseCodeEnum.Ö.namRu -> { result += MorseCodeEnum.Ö.code }
            MorseCodeEnum.Y.namEng,
            MorseCodeEnum.Y.namRu -> { result += MorseCodeEnum.Y.code }
            MorseCodeEnum.Z.namEng,
            MorseCodeEnum.Z.namRu -> { result += MorseCodeEnum.Z.code }
            MorseCodeEnum._1.namEng,
            MorseCodeEnum._1.namRu -> { result += MorseCodeEnum._1.code }
            MorseCodeEnum._2.namEng,
            MorseCodeEnum._2.namRu -> { result += MorseCodeEnum._2.code }
            MorseCodeEnum._3.namEng,
            MorseCodeEnum._3.namRu -> { result += MorseCodeEnum._3.code }
            MorseCodeEnum._4.namEng,
            MorseCodeEnum._4.namRu -> { result += MorseCodeEnum._4.code }
            MorseCodeEnum._5.namEng,
            MorseCodeEnum._5.namRu -> { result += MorseCodeEnum._5.code }
            MorseCodeEnum._6.namEng,
            MorseCodeEnum._6.namRu -> { result += MorseCodeEnum._6.code }
            MorseCodeEnum._7.namEng,
            MorseCodeEnum._7.namRu -> { result += MorseCodeEnum._7.code }
            MorseCodeEnum._8.namEng,
            MorseCodeEnum._8.namRu -> { result += MorseCodeEnum._8.code }
            MorseCodeEnum._9.namEng,
            MorseCodeEnum._9.namRu -> { result += MorseCodeEnum._9.code }
            MorseCodeEnum._0.namEng,
            MorseCodeEnum._0.namRu -> { result += MorseCodeEnum._0.code }
            MorseCodeEnum.DOT.namEng,
            MorseCodeEnum.DOT.namRu -> { result += MorseCodeEnum.DOT.code }
            MorseCodeEnum.COMMA.namEng,
            MorseCodeEnum.COMMA.namRu -> { result += MorseCodeEnum.COMMA.code }
            MorseCodeEnum.COLON.namEng,
            MorseCodeEnum.COLON.namRu -> { result += MorseCodeEnum.COLON.code }
            MorseCodeEnum.SEMICOLON.namEng,
            MorseCodeEnum.SEMICOLON.namRu -> { result += MorseCodeEnum.SEMICOLON.code }
            MorseCodeEnum.BRACKET_OPEN.namEng,
            MorseCodeEnum.BRACKET_OPEN.namRu -> { result += MorseCodeEnum.BRACKET_OPEN.code }
            MorseCodeEnum.BRACKET_CLOSE.namEng,
            MorseCodeEnum.BRACKET_CLOSE.namRu -> { result += MorseCodeEnum.BRACKET_CLOSE.code }
            MorseCodeEnum.APOSTROPHE.namEng,
            MorseCodeEnum.APOSTROPHE.namRu -> { result += MorseCodeEnum.APOSTROPHE.code }
            MorseCodeEnum.QUOTES.namEng,
            MorseCodeEnum.QUOTES.namRu -> { result += MorseCodeEnum.QUOTES.code }
            MorseCodeEnum.QUOTES_BRACKET_OPEN.namEng,
            MorseCodeEnum.QUOTES_BRACKET_OPEN.namRu -> { result += MorseCodeEnum.QUOTES_BRACKET_OPEN.code }
            MorseCodeEnum.QUOTES_BRACKET_CLOSE.namEng,
            MorseCodeEnum.QUOTES_BRACKET_CLOSE.namRu -> { result += MorseCodeEnum.QUOTES_BRACKET_CLOSE.code }
            MorseCodeEnum.DASH.namEng,
            MorseCodeEnum.DASH.namRu -> { result += MorseCodeEnum.DASH.code }
            MorseCodeEnum.SLASH.namEng,
            MorseCodeEnum.SLASH.namRu -> { result += MorseCodeEnum.SLASH.code }
            MorseCodeEnum.UNDERLINE.namEng,
            MorseCodeEnum.UNDERLINE.namRu -> { result += MorseCodeEnum.UNDERLINE.code }
            MorseCodeEnum.QUESTION_MARK.namEng,
            MorseCodeEnum.QUESTION_MARK.namRu -> { result += MorseCodeEnum.QUESTION_MARK.code }
            MorseCodeEnum.EXCLAMATION_MARK.namEng,
            MorseCodeEnum.EXCLAMATION_MARK.namRu -> { result += MorseCodeEnum.EXCLAMATION_MARK.code }
            MorseCodeEnum.PLUS.namEng,
            MorseCodeEnum.PLUS.namRu -> { result += MorseCodeEnum.PLUS.code }
            MorseCodeEnum.SECTION_SIGN.namEng,
            MorseCodeEnum.SECTION_SIGN.namRu -> { result += MorseCodeEnum.SECTION_SIGN.code }
            MorseCodeEnum._DOG.namEng,
            MorseCodeEnum._DOG.namRu -> { result += MorseCodeEnum._DOG.code }
        }
        return result
    }
}