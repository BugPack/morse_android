package com.example.morse.utils

import android.os.CountDownTimer
import kotlin.random.Random

val sourceEng = "abcdefghijklmnopqrstuvwxyz"
val sourceRu = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
var source = sourceEng

class StaminaGame {

    private val duration = 10000L
    private var letter = ""
    private var resultCallback: ((Boolean) -> Unit)? = null
    private var letterCallback: ((String, String) -> Unit)? = null
    private var progressCallback: ((Int) -> Unit)? = null
    private var gameInfoCallback: ((String) -> Unit)? = null
    private var newGameCallback: (() -> Unit)? = null

    private var timer: CountDownTimer? = null
    private var soundGenerator: SoundGenerator? = null

    private var win = 0
    private var loses = 0

    fun setProgressCallback(progressCallback: (Int) -> Unit) {
        this.progressCallback = progressCallback
    }

    fun setLetterCallback(letterCallback: (String, String) -> Unit) {
        this.letterCallback = letterCallback
    }

    fun setResultCallback(callback: (Boolean) -> Unit) {
        this.resultCallback = callback
    }

    fun setGameInfoCallback(callback: (String) -> Unit) {
        this.gameInfoCallback = callback
    }

    fun setNewGameCallback(callback: () -> Unit) {
        this.newGameCallback = callback
    }

    private fun randomLetter(): String {
        newGameCallback?.invoke()
        return source[Random.nextInt(0, source.length)].toString()
    }

    fun startGame() {
        soundGenerator = SoundGenerator()
        letter = randomLetter()
        updateInfo()
        returnLetterMorse(letter)
        timer?.cancel()
        timer = object : CountDownTimer(duration, 1000L) {
            override fun onFinish() {
                lose()
            }

            override fun onTick(millisUntilFinished: Long) {
                progressCallback?.invoke((millisUntilFinished / 100).toInt())
            }
        }.start()
    }

    private fun returnLetterMorse(letter: String) {
        val result = MorseHelper.letterToMorse(letter)
        letterCallback?.invoke(letter, result)
    }

    private fun updateInfo() {
        gameInfoCallback?.invoke("win: $win  loses: $loses")
    }

    fun checkLetter(letter: String) {
        if (this.letter == letter) {
            win++
            resultCallback?.invoke(true)
            timer?.cancel()
            startGame()
        } else {
            lose()
        }
        updateInfo()
    }

    private fun lose() {
        loses++
        resultCallback?.invoke(false)

        progressCallback?.invoke(100)

        timer?.cancel()
        timer = object : CountDownTimer(2000L, 1000L) {
            override fun onFinish() {
                startGame()
            }
            override fun onTick(millisUntilFinished: Long) {
            }
        }.start()
    }

    fun stopGame() {
        timer?.cancel()
        soundGenerator = null
    }

    fun triggerLanguage() {
        source = if (source == sourceEng) sourceRu else sourceEng
        startGame()
    }
}