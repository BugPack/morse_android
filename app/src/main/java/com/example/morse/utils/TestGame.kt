package com.example.morse.utils

import com.example.morse.entity.LetterEntity
import kotlin.random.Random

class TestGame {
    private var letter = ""
    private var letterCallback: ((String) -> Unit)? = null
    private var resultCallback: ((Boolean) -> Unit)? = null
    private var answersCallback: ((List<LetterEntity>) -> Unit)? = null

    fun setLetterCallback(callback: (String) -> Unit) {
        letterCallback = callback
    }

    fun setResultCallback(callback: (Boolean) -> Unit) {
        resultCallback = callback
    }

    fun setAnswersCallback(callback: (List<LetterEntity>) -> Unit) {
        answersCallback = callback
    }

    private fun randomLetter(): String {
        return source[Random.nextInt(0, source.length)].toString()
    }

    fun startGame() {
        letter = randomLetter()
        letterCallback?.invoke(letter)
        val answers = arrayListOf<LetterEntity>()
        while (answers.size < 3) {
            val letterError = randomLetter()
            if (letterError != letter) {
                answers.add(LetterEntity(letterError, MorseHelper.letterToMorse(letterError)))
            }
        }
        answers.add(LetterEntity(letter, MorseHelper.letterToMorse(letter)))
        answers.sortedWith(compareBy(LetterEntity::letter))
        answersCallback?.invoke(answers)
    }

    fun checkAnswer(letterEntity: LetterEntity) {
        if (letterEntity.letter == letter) {
            resultCallback?.invoke(true)
        } else {
            resultCallback?.invoke(false)
        }
    }
}