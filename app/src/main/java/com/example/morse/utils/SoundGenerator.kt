package com.example.morse.utils

import android.content.Context
import android.media.SoundPool
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.pawegio.kandroid.d
import java.util.*

class SoundGenerator {

    data class Sound(val resId: Int, val duration: Long)

    // 0f - 1.0f
    private var volume = 0.5f
    private var soundPool = SoundPool.Builder().setMaxStreams(1).build()
    private var arrayDeque = ArrayDeque<Sound>()

    private fun playSound(context: Context, soundId: Int, duration: Long, callback: () -> Unit) {
        try {
            soundPool = SoundPool.Builder().setMaxStreams(1).build()
            soundPool?.setOnLoadCompleteListener { soundPool, sampleId, status ->
                soundPool?.play(sampleId, volume, volume, 1, 0, 1f)
            }
            soundPool?.load(context, soundId, 0)

            Handler(Looper.getMainLooper()).postDelayed({
                callback()
            }, duration)
        } catch (e: Exception) {
            Log.d("_log", "${e.message}")
        }
    }

    fun playSoundSet(context: Context, soundList: List<Sound>) {
        arrayDeque.clear()
        arrayDeque.addAll(soundList)

        play(context)
    }

    private fun play(context: Context) {
        try {
            if (arrayDeque.isNotEmpty()) {
                val sound = arrayDeque.first
                playSound(context, sound.resId, sound.duration) {
                    if (arrayDeque.isNotEmpty()) {
                        arrayDeque.removeFirst()
                        play(context)
                    }
                }
            }
        } catch (e: Exception) {
            d("_log", "${e.message}")
        }
    }

    fun setVolume(volume: Int) {
        when {
            volume in 0..100 -> {
                this.volume = volume / 100f
            }
            volume < 0 -> {
                this.volume = 0f
            }
            volume > 100 -> {
                this.volume = 1f
            }
        }
        d("_log", "volume = ${this.volume}")
    }
}