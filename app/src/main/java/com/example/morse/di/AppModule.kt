package com.example.morse.di

import com.example.morse.common.App
import com.example.morse.viewmodel.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

val appModule = module {
    single { Cicerone.create() }
    single<Router> { get<Cicerone<Router>>().router }
    single<NavigatorHolder> { get<Cicerone<Router>>().navigatorHolder }

    viewModel { WritingViewModel(App.INSTANCE) }
    viewModel {
        MenuViewModel(
            router = get()
        )
    }
    viewModel { StaminaViewModel(context = App.INSTANCE) }
    viewModel { ABCViewModel() }
    viewModel { TestViewModel() }
}