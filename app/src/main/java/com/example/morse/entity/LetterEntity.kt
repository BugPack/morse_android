package com.example.morse.entity

data class LetterEntity(
    val letter: String,
    val letterMorse: String
)