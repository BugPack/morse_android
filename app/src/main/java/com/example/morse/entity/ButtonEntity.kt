package com.example.morse.entity

data class ButtonEntity(
    var text: String,
    var onClick: () -> Unit
)