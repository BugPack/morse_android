package com.example.morse.entity

enum class LanguageEnum {
    ENG,
    RU
}